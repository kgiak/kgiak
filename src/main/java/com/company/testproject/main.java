package com.company.testproject;

import com.company.testproject.model.Movie;
import com.company.utils.MovieUtils;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;

public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if(args.length!=4){
            System.out.println("");
            System.out.println("Please provide 4 arguments to run this program.");
            System.out.println("-----INFO-----");
            System.out.println("The first three arguments are input filepaths.");
            System.out.println("Each filepath corresponds to a person watch list.");
            System.out.println("The forth argument is the result list filepath.");
            System.out.println("--------------");
        }else{
            System.out.println("EXECUTING TASK 1 & 2:");
            try{
                taskOneAndTwo(args);
                //similarly we can code other methods and provide differenct kind of statistics.
                //this comment is regarding task 3 4 and 5. 
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
    
    /**
     * 
     * @param args
     * @throws Exception 
     */
    private static void taskOneAndTwo(String[] args) throws Exception{
        List<Movie> movieList = new ArrayList<Movie>();
        for(int i=0; i<3; i++){//parse the input filepaths
                try (Stream<String> stream = Files.lines(Paths.get(args[i]))) {
                    System.out.println("");
                    System.out.println("- READING : "+args[i]);
                    int filenameStartLoc = args[i].lastIndexOf("\\");
                    String reviewer =  args[i].substring(filenameStartLoc+1).replace(".txt", "");
                    System.out.println("- Reviewer : "+reviewer);
                    //parse reviewer files
                    stream.forEach(x -> {
                        String[] movieLineArr = x.split(" ");
                        if(movieLineArr.length!=5){
                            System.out.println("ERROR reading file - Invalid arguments length.");
                        }else{
                            if(!movieLineArr[2].endsWith("min") || !movieLineArr[3].endsWith("min")){
                                System.out.println("ERROR reading file - Invalid records format.");
                            }else{
                                String movieMins = movieLineArr[2].substring(0, movieLineArr[2].length() - 3);
                                String movieMinsWatched = movieLineArr[3].substring(0, movieLineArr[3].length() - 3);
                                int movieMinutes = 0;
                                int movieMinutesWatched = 0;
                                try{movieMinutes = Integer.parseInt(movieMins);}catch(Exception e){}
                                try{movieMinutesWatched = Integer.parseInt(movieMinsWatched);}catch(Exception e){}
                                if(movieMinutes==0 || movieMinutesWatched==0){
                                    System.out.println("ERROR reading file - Invalid record values.");
                                }else{
                                    Movie movie = new Movie(movieLineArr[1], movieMinutes, movieLineArr[4], reviewer, movieLineArr[0], movieMinutesWatched);
                                    movieList.add(movie);
                                }
                            }
                        }
                    });
                    System.out.println("");
                    System.out.println(" - TASK 2 results : ");
                    System.out.println(" - Average minutes watched : "+MovieUtils.averageMinutesWatched(movieList));
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
            }
            Map<String, Integer> movieMap = MovieUtils.orderMoviesByMinutes(movieList);
            System.out.println(" - TASK 1 results : ");
            for (Entry<String, Integer> entry : movieMap.entrySet()){
                System.out.println(entry.getValue() + "mins : "+ entry.getKey());
            }
    }
}
