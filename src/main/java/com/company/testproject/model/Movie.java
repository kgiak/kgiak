package com.company.testproject.model;

public class Movie {
    String title;
    Integer minutes;
    String genre;
    String reviewer;
    String dayWatched;
    Integer minutesWatched;

    public Movie(String title, Integer minutes, String genre, String reviewer, String dayWatched, Integer minutesWatched) {
        this.title = title;
        this.minutes = minutes;
        this.genre = genre;
        this.reviewer = reviewer;
        this.dayWatched = dayWatched;
        this.minutesWatched = minutesWatched;
    }
    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getMinutes() {
        return minutes;
    }

    public void setMinutes(Integer minutes) {
        this.minutes = minutes;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getReviewer() {
        return reviewer;
    }

    public void setReviewer(String reviewer) {
        this.reviewer = reviewer;
    }

    public String getDayWatched() {
        return dayWatched;
    }

    public void setDayWatched(String dayWatched) {
        this.dayWatched = dayWatched;
    }
    
    public Integer getMinutesWatched() {
        return minutesWatched;
    }

    public void setMinutesWatched(Integer minutesWatched) {
        this.minutesWatched = minutesWatched;
    }
    
}
