/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.utils;

import com.company.testproject.model.Movie;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public class MovieUtils {
    
    public static Map<String,Integer> orderMoviesByMinutes (List<Movie> movieList) throws Exception{
        HashMap<String, Integer> movieMap = new HashMap<String, Integer>();
        for(Movie movie : movieList){
            movieMap.put(movie.getTitle(), movie.getMinutes());
        }
        return sortHashMapByValues(movieMap);
    }
    
    /**
     * Returns the average minutes watched within a given movie list.
     * @param movieList The provided movie List
     * @return The average minutes watched as an Integer type
     * @throws Exception 
     */
    public static Integer averageMinutesWatched(List<Movie> movieList) throws Exception{
        int minutesWatchedPercentage = 0;
        for(Movie movie : movieList){
            minutesWatchedPercentage += (movie.getMinutesWatched()*100)/movie.getMinutes();
        }
        return minutesWatchedPercentage/movieList.size();
    }
    
    /**
     * Sorts a Hashmap according to the provided values.
     * @param givenMap The provided Map
     * @return The sorted new LinkedHashMap
     * @throws Exception 
     */
    public static LinkedHashMap<String, Integer> sortHashMapByValues(HashMap<String, Integer> givenMap) throws Exception{
        List<String> mapKeys = new ArrayList<>(givenMap.keySet());
        List<Integer> mapValues = new ArrayList<>(givenMap.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);

        LinkedHashMap<String, Integer> sortedMap = new LinkedHashMap<>();

        Iterator<Integer> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            Integer val = valueIt.next();
            Iterator<String> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                String key = keyIt.next();
                Integer comp1 = givenMap.get(key);
                Integer comp2 = val;

                if (comp1.equals(comp2)) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }
        }
        return sortedMap;
    }
}
